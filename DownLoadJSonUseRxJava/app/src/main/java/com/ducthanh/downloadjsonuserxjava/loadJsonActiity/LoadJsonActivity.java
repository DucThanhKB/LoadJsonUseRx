package com.ducthanh.downloadjsonuserxjava.loadJsonActiity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.ducthanh.downloadjsonuserxjava.R;
import com.ducthanh.downloadjsonuserxjava.retrofit.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoadJsonActivity extends AppCompatActivity implements LoadJsonInterface.LoadJsonViewInterface {

    @BindView(R.id.text_name)
    TextView textTen;
    @BindView(R.id.text_company)
    TextView textCompany;

    private LoadJsonPresenter mLoadJsonPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_json);
        ButterKnife.bind(this);
        mLoadJsonPresenter = new LoadJsonPresenter(this);
    }
    
    @OnClick(R.id.button_load_json)
    public void buttonLoadJsonPressed() {
      mLoadJsonPresenter.handleLoadJson();
    }

    @Override
    public void upDateView(User user) {
        textTen.setText(user.getName());
        textCompany.setText(user.getCompany());
    }

    @Override
    public void showError() {
        Toast.makeText(LoadJsonActivity.this, "Error", Toast.LENGTH_SHORT).show();
    }
}