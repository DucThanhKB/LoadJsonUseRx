package com.ducthanh.downloadjsonuserxjava.retrofit;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by DucThanh on 7/18/2017.
 */

public interface RetrofitInterface {
    @GET("/users/luckymenkn")
    Observable<User> getUser();

}
