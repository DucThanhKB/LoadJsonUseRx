package com.ducthanh.downloadjsonuserxjava.loadJsonActiity;

import com.ducthanh.downloadjsonuserxjava.retrofit.User;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by DucThanh on 7/18/2017.
 */

public class LoadJsonPresenter implements LoadJsonInterface.LoadJsonPresenterInterface {
    LoadJsonModel mModel;
    LoadJsonInterface.LoadJsonViewInterface mView;


    public LoadJsonPresenter(LoadJsonInterface.LoadJsonViewInterface mView) {
        this.mView = mView;
        mModel = new LoadJsonModel(this);
    }
    public void handleLoadJson(){

        Disposable disposable = mModel.loadJson()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<User>() {
                    @Override
                    public void accept(User user) throws Exception {
                        mView.upDateView(user);
                }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mView.showError();
                    }
                });
    }
}
