package com.ducthanh.downloadjsonuserxjava.loadJsonActiity;

import com.ducthanh.downloadjsonuserxjava.retrofit.User;

/**
 * Created by DucThanh on 7/18/2017.
 */

public interface LoadJsonInterface {
    interface LoadJsonViewInterface {
        void upDateView(User user);
        void showError();
    }

    interface LoadJsonPresenterInterface {

    }
}
