package com.ducthanh.downloadjsonuserxjava.loadJsonActiity;

import com.ducthanh.downloadjsonuserxjava.retrofit.User;
import com.ducthanh.downloadjsonuserxjava.retrofit.RetrofitClient;
import com.ducthanh.downloadjsonuserxjava.retrofit.RetrofitInterface;

import io.reactivex.Observable;

/**
 * Created by DucThanh on 7/18/2017.
 */

public class LoadJsonModel {
    private LoadJsonInterface.LoadJsonPresenterInterface mPresenter;

    public LoadJsonModel(LoadJsonInterface.LoadJsonPresenterInterface mPresenter) {
        this.mPresenter = mPresenter;
    }
    public Observable<User> loadJson(){
        RetrofitClient retrofitClient = new RetrofitClient();
        RetrofitInterface retrofitInterface = retrofitClient.getRetrofitInterface();
        return retrofitInterface.getUser();
    }
}
